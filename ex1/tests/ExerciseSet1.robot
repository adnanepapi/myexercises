*** Settings ***
# TODO: Exercise 1: Import Selenium2Library to successfully run Exercise 1 Test
Library    Selenium2Library

#TODO: Exercise 2: Create a robot file named data.robot, declare a variable called browser and save a browser name to it, and import the data.robot resource here
Resource    data.robot

*** Variables ***
# TODO: Declare Variable for URL here
${URL}      https://www.google.ca/

*** Test Cases ***

Exercise 1 Test  # Should run if Selenium2Library is imported correctly under Settings
	Open Browser        https://www.google.ca/    chrome
    Title Should Be     Google


Exercise 2 Test
    [Documentation]  TODO: Exercise 2: Copy the test case script used in Exercise 1, and replace the hardcoded URL(declared in this file)
    ...  and browser(declared in data.robot, add Documentation header in settings to specify its for keywords) with variables
    Open Browser        ${URL}    ${BROWSER}
    Title Should Be     Google


Exercise 3 Test
    [Documentation]  TODO: Using Selenium2Library as a resource as well as everything learned from Exercises 1 and 2,
    ...  create a script that will open up the browser, navigate to the Iris Login URL and confirm the title,
    ...  login as a customer admin and verify that the dashboard page is open by checking the title.
    ...  For exercise purposes, try to open browser to random site (i.e. google.com) then navigate to Iris Login URL
# Hint: Keywords needed: Open Browser, Input Text, Title Should be, Go to, Location Should Be
    Open Browser   ${URL}    ${BROWSER}
    Go to   https://staging1-iris.t2systems.com/
#    Input Text  xpath=//*[@id="username"]  adnane.agbetra
    Input Text  css=#username  adnane.agbetra
    Input Text   css=#password  ${PASSWORD}
#    Input Password  xpath=//*[@id="password"]  ${PASSWORD}
    Click Button  id=submit
    Title Should Be   Digital Iris - Main (Dashboard)
    Sleep   3s
    Close Browser


css=#username
css=#password
submit

Exercise 4 Test
    [Documentation]  TODO: Robust the test script written in Exercise 3 into re-usable keywords and define it under Keywords,
    ...  either internally or as an external file. Then construct the same test as Exercise 3, using only the newly defined keywords.
    ...  *Bonus* Move the keywords than to the external data.robot file created in Exercise 2, under the same Keywords header
    ...  and then re-run the test. If it passed before, it should also pass here as well.
    ...  Keyword for submitting credentials will already be provided as a sample
    Open Browser to Iris Login Page
    Input Username
    Input Password
    Submit Credentials
    Customer Admin Welcome Page Should Be Open
    Sleep   3s
    Close Browser


*** Keywords ***

Submit Credentials
    Click Button    submit

Input Username
#TODO: Exercise 4: Create Keyword with one argument that takes in a username and inputs it into the username field of Iris Login
    Input Text  css=#username  adnane.agbetra
Input Password
#TODO: Exercise 4: Create Keyword with one argument that takes in a password and inputs it into the password field of Iris Login
    Input Text   css=#password  ${PASSWORD}

Login Page Should Be Open
#TODO: Exercise 4: Create Keyword that simply checks the current page location is the url of the iris login page and the title is Digital Iris - Login
    Location Should Be  https://staging1-iris.t2systems.com/
    Title Should Be   Digital Iris - Sign in

Go To Login Page
#TODO: Exercise 4: Create Keyword that simply navigates browser to login page
    Go to   https://staging1-iris.t2systems.com/

Customer Admin Welcome Page Should Be Open
#TODO: Exercise 4: Create keyword that after successful login, verifies the page location and title is at the dashboard
    Location Should Be  https://staging1-iris.t2systems.com/secure/dashboard/index.html
    Title Should Be   Digital Iris - Main (Dashboard)

Open Browser to Iris Login Page
#TODO: Exercise 4: Create Keyword that opens up the browser, maximizes the browser window, set selenium speed (already provided),
#TODO: and navigate to Iris Login URL (Using the Go To Login Page keyword. (Note: Keywords can call other keywords so long as imported correctly in Settings))
    Open Browser  https://www.google.ca/    chrome
    Maximize Browser Window
    Set Selenium Speed  0
    Go To Login Page


#	Set Selenium Speed    0