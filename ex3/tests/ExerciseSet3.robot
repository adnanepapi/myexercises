*** Settings ***
Library           Selenium2Library


Suite Setup       Open Browser to Iris Login Page
Suite Teardown    Close Browser

#TODO: Add a Test Setup step to automatically reload the page before every test
Test Setup   Reload Page

#TODO: Add a test template step to be used as a template for runnning each test
Test Template   Login with invalid credentials should fail


*** Variables ***
${SERVER}         staging1-iris.t2systems.com
${BROWSER}        firefox
${LOGIN URL}      https://${SERVER}/

*** Test Cases ***

# TODO: Re-factor these test steps into a re-usable template and change the test cases into a tabular format

Invalid Login 1  billyboy  1233213123


Invalid Login 2  admin@oranj  sdfsdfsfdsfsdfsdfsd


Invalid Login 3  werwrwerfs    Password$1


Invalid Login 4  admin@oranj   ${EMPTY}


*** Keywords ***
Alert Message Should Appear
	Element Should Be Visible    xpath=.//*[@id='messageResponseAlertBox']/section/h4/strong[contains(text(), 'Attention!')]

Open Browser to Iris Login Page
    Open Browser    ${LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    0
    Login Page Should Be Open

Login Page Should Be Open
    Title Should Be    Digital Iris - Sign in


#TODO: Create new keyword that will act as a template for testing
Login with invalid credentials should fail
    [Arguments]    ${username}     ${password}
    Input Text     css=#username   ${username}
    Input Text     css=#password   ${password}
    Click Button   submit
    Alert Message Should Appear

  