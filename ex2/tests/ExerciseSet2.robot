*** Settings ***
Library           Selenium2Library

#TODO: Add a Suite Setup step to automatically open the browser up for UI Testing
Suite Setup     Open Browser    https://www.google.ca/   firefox


#TODO: Add a Suite Teardown step to automatically close the browser when suite is finished
Suite Teardown  Close Browser

*** Variables ***


*** Test Cases ***


#TODO: Change all Open Browser keywords to 'Go to'

Browser Test 1
	Go To        https://www.google.ca/
    Title Should Be     Google
    Close Browser

Browser Test 2
	Go To        https://www.youtube.com/
    Title Should Be     YouTube
    Close Browser

Browser Test 3
	Go To        https://www.python.org/
    Title Should Be     Welcome to Python.org
    Close Browser

Browser Test 4
	Go To        http://robotframework.org/
    Title Should Be     Robot Framework
    Close Browser

Browser Test 5
	Go To        https://staging1-iris.t2systems.com/
    Title Should Be     Digital Iris - Sign in
    Close Browser

Browser Test 6
	Go To        http://www.seleniumhq.org/
    Title Should Be     Selenium - Web Browser Automation
    Close Browser


*** Keywords ***




